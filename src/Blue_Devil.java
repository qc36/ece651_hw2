package src;

import java.util.ArrayList;

public class Blue_Devil extends Person{
	String Major;
	String Occupation;
	String Program;
	Blue_Devil(String[] name, String worke, ArrayList<String> hobby, String nationality, boolean sex,String undergrad, int age, String major, String occupation, String program){
		super(name, worke, hobby, nationality, sex, undergrad,age);
		Major=major;
		Occupation=occupation;
		Program=program;
	}
	Blue_Devil() {		// TODO Auto-generated constructor stub
	}
	void discribe(){
		System.out.print(this.Name[0]+this.Name[1]);
		System.out.print(" is from "+this.Nationality);
		System.out.print(" and is a ");
		if(this.Occupation!="Professor") {
			System.out.print(this.Program+ " ");
		}
		System.out.print(this.Major+ " ");
		System.out.print(this.Occupation+". ");
		if(this.Sex==true) {
			System.out.print("He ");
		}
		else {
			System.out.print("She ");
		}
		if(this.Work_exp=="None") {
			System.out.print("does not have work experience. ");
		}
		else {
			System.out.print("used work in "+this.Work_exp+". ");
		}
		if(this.Undergrad!="None") {
			if(this.Sex==true) {
				System.out.print("He received his undergraduate from "+this.Undergrad+". ");
			}
			else {
				System.out.print("She received her undergraduate from "+this.Undergrad+". ");
			}
			
		}
		System.out.print("When not in class, "+this.Name[0]+" enjoys");
		for(int i=0;i<this.Hobby.size();i++) {
			if(i==this.Hobby.size()-1)
				System.out.print(" and");
			System.out.print(" "+this.Hobby.get(i));
			if(i<this.Hobby.size()-2)
				System.out.print(",");			
		}
		System.out.println(".");
	}
	void AddSearch(String Attribute){
        switch (Attribute) {
        	case "Name":
        		System.out.println("Name: "+this.Name[0]+" "+ this.Name[1]);
            	break;
        	case "Major":
        		System.out.println("Major: "+this.Major);
            	break;
        	case "Work_exp":
        		System.out.println("Work Experience: "+this.Work_exp);
            	break;
        	case "Nationality":
        		System.out.println("Nationality: "+this.Nationality);
            	break;
        	case "Hobby":
        		System.out.println("Hobby: ");
        		for(int i=0;i<this.Hobby.size();i++) {
        			System.out.println(" "+this.Hobby.get(i));		
        		}
            	break;
        	case "Sex":
        		System.out.print("Sex: ");
        		if(this.Sex==true)
        			System.out.print("Male");
        		else
        			System.out.print("Female");
            	break;
        	case "Age":
        		System.out.println("Age: "+this.Age);
            	break;
        	case "Undergrad":
        		System.out.println("Undergraduate: "+this.Undergrad);
            	break;
        	case "Occupation":
        		System.out.println("Occupation: "+this.Occupation);
            	break;
        	case "Program":
        		System.out.println("Program: "+this.Program);
            	break;
            default:
            	System.out.println("No such Attribute!");
        }
	}
}

package src;
import java.util.ArrayList;
import java.util.Scanner;
import src.Blue_Devil;
public class main {
		// TODO Auto-generated method stub
	static ArrayList<Blue_Devil> OrigionalData(){
		
		ArrayList<Blue_Devil> AllData=new ArrayList<Blue_Devil>();	
		
		String[] name_qc= new String[2];
		name_qc[0]="Qingxuan";
		name_qc[1]="Chen";
	    ArrayList<String> hobby_qc= new ArrayList<String>();
	    hobby_qc.add("listening music");
	    hobby_qc.add("reading book");
	    hobby_qc.add("cooking");
	    Blue_Devil Qingxuan_Chen= new Blue_Devil(name_qc, "None", hobby_qc, "China", false, "CJLU", 22,"ECE", "Student", "MS");
	    AllData.add(Qingxuan_Chen);
	    
	    String[] name_af= new String[2];
	    name_af[0]="Adel";
	    name_af[1]="Fahmy";
	    ArrayList<String> hobby_af= new ArrayList<String>();
	    hobby_af.add("playing tennis");
	    hobby_af.add("biking");
	    hobby_af.add("garding");
	    hobby_af.add("cooking");
	    Blue_Devil Adel_Fahmy= new Blue_Devil(name_af, "IBM", hobby_af, "US", true, "UC State", 57,"ECE", "Professor", "None");
	    AllData.add(Adel_Fahmy);
	    
	    String[] name_rt= new String[2];
	    name_rt[0]="Ric";
	    name_rt[1]="Telford";
	    ArrayList<String> hobby_rt= new ArrayList<String>();
	    hobby_rt.add("playing golf");
	    hobby_rt.add("playing volleyball");
	    hobby_rt.add("swimming");
	    hobby_rt.add("biking");
	    Blue_Devil Ric_Telford= new Blue_Devil(name_rt, "IBM", hobby_rt, "US", true, "Trinity University", 57,"ECE", "Professor", "None");
	    AllData.add(Ric_Telford);
	    
	    String[] name_yy= new String[2];
	    name_yy[0]="Yuanyuan";
	    name_yy[1]="Yu";
	    ArrayList<String> hobby_yy= new ArrayList<String>();
	    hobby_yy.add("playing baseball");
	    hobby_yy.add("fensing");
	    Blue_Devil Yuanyuan_Yu= new Blue_Devil(name_yy, "None", hobby_yy, "China", false, "ECUST", 23,"ECE", "Student", "MEng");
	    AllData.add(Yuanyuan_Yu);
	    
	    String[] name_yl= new String[2];
	    name_yl[0]="You";
	    name_yl[1]="Lyu";
	    ArrayList<String> hobby_yl= new ArrayList<String>();
	    hobby_yl.add("listening music");
	    hobby_yl.add("traveling");
	    hobby_yl.add("history");
	    Blue_Devil You_Lyu= new Blue_Devil(name_yl, "None", hobby_yl, "China", false, "Nottingham University", 23,"ECE", "Student", "MS");
	    AllData.add(You_Lyu);
	    
	    String[] name_lc= new String[2];
	    name_lc[0]="Lei";
	    name_lc[1]="Chen";
	    ArrayList<String> hobby_lc= new ArrayList<String>();
	    hobby_lc.add("climbing");
	    hobby_lc.add("animals");
	    Blue_Devil Lei_Chen= new Blue_Devil(name_yl, "None", hobby_yl, "China", false, "KAIST", 23,"ECE", "Student", "MS");
	    AllData.add(Lei_Chen);
	    
	    String[] name_ss= new String[2];
	    name_ss[0]="Shalin";
	    name_ss[1]="Shah";
	    ArrayList<String> hobby_ss= new ArrayList<String>();
	    hobby_ss.add("body building");
	    hobby_ss.add("dancing");
	    Blue_Devil Shalin_Shah= new Blue_Devil(name_ss, "None", hobby_ss, "India", false, "DA-IICT", 23,"ECE", "Student", "PHD");
	    AllData.add(Shalin_Shah);
	    
	    String[] name_zl= new String[2];
	    name_zl[0]="Zhongyu";
	    name_zl[1]="Li";
	    ArrayList<String> hobby_zl= new ArrayList<String>();
	    hobby_zl.add("playing basketball");
	    hobby_zl.add("NBA");
	    Blue_Devil Zhongyu_Li= new Blue_Devil(name_ss, "None", hobby_zl, "China", false, "SouthEast University", 23,"ECE", "Student", "MS");
	    AllData.add(Zhongyu_Li);
	    
	    return AllData;
	    
	}
	static Blue_Devil AddRecords(){
		
		Blue_Devil newrec=new Blue_Devil();
		System.out.println("Input first name:");
		Scanner in = new Scanner(System.in);
		newrec.Name[0]=in.next();
		newrec.Name[1]=in.next();
		System.out.println("Input Work Experience:");
		newrec.Work_exp=in.next();
		System.out.println("Input Nationality:");
		newrec.Nationality=in.next();
		System.out.println("Input Occupation:");
		newrec.Occupation=in.next();
		System.out.println("Input Program:");
		newrec.Program=in.next();
		System.out.println("Input Age:");
		newrec.Age=in.nextInt();
		System.out.println("Input Sex:(f or m)");
		String insex;
		do{
			insex=in.nextLine();
			System.out.println(insex);
			if (insex.equals("m")) {
				newrec.Sex=true;
			}
			else if (insex.equals("f")) {
				newrec.Sex=false;
			}
			else {
				System.out.println("Please input valid sex");
			}
		}while(!insex.equals("m")&&!insex.equals("f"));
		System.out.println("Input Undergraduate:");
		newrec.Undergrad=in.next();
		System.out.println("Input Major:");
	    newrec.Major=in.next();
		System.out.println("Input Hobby:");
		String hobby;
		while(in.hasNext()) {
			hobby=in.next();
			newrec.Hobby.add(hobby);
		}
		in.close();
		System.out.println("Add Successfully");
	    return newrec;
	}
	static  void DeleteRecords(ArrayList<Blue_Devil> data,String firstName, String lastName){
		for(int i=0;i<data.size();i++) {
			if(firstName.equals(data.get(i).Name[0])&&lastName.equals(data.get(i).Name[1])) {
				data.remove(i);
				System.out.println("Delete Successfully");
				return;
			}
		}
		System.out.println("Cannot find this person");
	}
	static void whoIs(ArrayList<Blue_Devil> data,String firstName, String lastName) {
		for(int i=0;i<data.size();i++) {
			if(firstName.equals(data.get(i).Name[0])&&lastName.equals(data.get(i).Name[1])) {
				data.get(i).discribe();
				return;
			}
		}
		System.out.println("Cannot find this person");
	}
	static void AttributeIs(ArrayList<Blue_Devil> data,String firstName, String lastName,String Attribute) {
		for(int i=0;i<data.size();i++) {
			if(firstName.equals(data.get(i).Name[0])&&lastName.equals(data.get(i).Name[1])) {
				data.get(i).AddSearch(Attribute);
				return;
			}
		}
		System.out.println("Cannot find this person");
	}
	public static void main(String[] args) {
		ArrayList<Blue_Devil> AllData=OrigionalData();
		while(true) {
		System.out.println("Input the command:(Search/Attribute/Add/Delete)");	
		Scanner in = new Scanner(System.in);
		String Command=in.nextLine();
		switch(Command) {
		case "Search": 
			System.out.println("Input the first name you search:");
	    	String FirstName=in.nextLine();
	    	System.out.println("Input the last name you search:");
	    	String LastName=in.nextLine();
	    	System.out.println("Discribe of the person you search:");
	    	whoIs(AllData,FirstName,LastName);
	    	break;
		
		case "Attribute":
			System.out.println("Input the first name you search:");
	    	FirstName=in.nextLine();
	    	System.out.println("Input the last name you search:");
	    	LastName=in.nextLine();
	    	System.out.println("The attribute you want to search:");
	    	String Attribute=in.nextLine();
	    	System.out.println("Discribe of the person you search:");
	    	AttributeIs(AllData,FirstName,LastName,Attribute);
	    	break;
	    
		case "Add":
			System.out.println("Input the person you want to add to record:");
			Blue_Devil newperson= AddRecords();
			AllData.add(newperson);
			System.out.println("Discribe of the person you add:");
			whoIs(AllData,newperson.Name[0],newperson.Name[1]);
			break;
		
		case "Delete":
			System.out.println("Input the first name you search:");
			FirstName=in.nextLine();
			System.out.println("Input the last name you search:");
			LastName=in.nextLine();
			System.out.println("Discribe of the person you search:");
			DeleteRecords(AllData,FirstName,LastName);
			break;
		default:
			System.out.println("Please input valid command");
			}  
		}
	}

}
